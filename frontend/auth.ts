import NextAuth from "next-auth";
import "next-auth/jwt";

import Apple from "next-auth/providers/apple";
import Auth0 from "next-auth/providers/auth0";
import AzureB2C from "next-auth/providers/azure-ad-b2c";
import BoxyHQSAML from "next-auth/providers/boxyhq-saml";
import Cognito from "next-auth/providers/cognito";
import Coinbase from "next-auth/providers/coinbase";
import Discord from "next-auth/providers/discord";
import Dropbox from "next-auth/providers/dropbox";
import Facebook from "next-auth/providers/facebook";
import GitHub from "next-auth/providers/github";
import GitLab from "next-auth/providers/gitlab";
import Google from "next-auth/providers/google";
import Hubspot from "next-auth/providers/hubspot";
import Keycloak from "next-auth/providers/keycloak";
import LinkedIn from "next-auth/providers/linkedin";
import Netlify from "next-auth/providers/netlify";
import Okta from "next-auth/providers/okta";
import Passage from "next-auth/providers/passage";
// import Passkey from "next-auth/providers/passkey"
import Pinterest from "next-auth/providers/pinterest";
import Reddit from "next-auth/providers/reddit";
import Slack from "next-auth/providers/slack";
import Spotify from "next-auth/providers/spotify";
import Twitch from "next-auth/providers/twitch";
import Twitter from "next-auth/providers/twitter";
import WorkOS from "next-auth/providers/workos";
import Zoom from "next-auth/providers/zoom";
import CredentialsProvider from "next-auth/providers/credentials";

import { createStorage } from "unstorage";
import { UnstorageAdapter } from "@auth/unstorage-adapter";
import type { Account, NextAuthConfig, User } from "next-auth";

import memoryDriver from "unstorage/drivers/memory";
import axios from "axios";

// const storage = createStorage({
//   driver: memoryDriver(), // !typeof localStorage ? memoryDriver() : localStorageDriver({ base: 'app:' }),
// });

const config = {
  theme: { logo: "https://authjs.dev/img/logo-sm.png" },
  // adapter: UnstorageAdapter(storage),
  providers: [
    // Apple,
    // Auth0,
    // AzureB2C({
    //   clientId: process.env.AUTH_AZURE_AD_B2C_ID,
    //   clientSecret: process.env.AUTH_AZURE_AD_B2C_SECRET,
    //   issuer: process.env.AUTH_AZURE_AD_B2C_ISSUER,
    // }),
    // BoxyHQSAML({
    //   clientId: "dummy",
    //   clientSecret: "dummy",
    //   issuer: process.env.AUTH_BOXYHQ_SAML_ISSUER,
    // }),
    // Cognito,
    // Coinbase,
    // Discord,
    // Dropbox,
    // Facebook,
    // GitHub,
    CredentialsProvider({
      // The name to display on the sign in form (e.g. "Sign in with...")
      name: "Credentials",
      // `credentials` is used to generate a form on the sign in page.
      // You can specify which fields should be submitted, by adding keys to the `credentials` object.
      // e.g. domain, username, password, 2FA token, etc.
      // You can pass any HTML attribute to the <input> tag through the object.
      credentials: {
        username: {
          label: "Username",
          type: "text",
          placeholder: "my_nickname",
        },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        let res;
        try {
          res = (
            await axios.post(
              `${process.env.NEXT_PUBLIC_BACKEND_URL}/api/validate-user-creds`,
              {
                username: credentials.username,
                password: credentials.password,
              },
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            )
          ).data;
        } catch (error: any) {
          console.log(error.message);
          return null;
        }
        if (res.status === "ok") {
          // Any object returned will be saved in `user` property of the JWT
          return res.user;
        } else {
          // If you return null then an error will be displayed advising the user to check their details.
          return null;
        }
      },
    }),
    // GitLab,
    // Google,
    // Hubspot,
    // Keycloak,
    // LinkedIn,
    // Netlify,
    // Okta,
    // Passkey({
    //   formFields: {
    //     email: {
    //       label: "Username",
    //       required: true,
    //       autocomplete: "username webauthn",
    //     },
    //   },
    // }),
    // Passage,
    // Pinterest,
    // Reddit,
    // Slack,
    // Spotify,
    // Twitch,
    // Twitter,
    // WorkOS({
    //   connection: process.env.AUTH_WORKOS_CONNECTION!,
    // }),
    // Zoom,
  ],
  basePath: "/auth",
  // callbacks: {
  //   authorized({ request, auth }) {
  //     const { pathname } = request.nextUrl;
  //     if (pathname === "/middleware-example") return !!auth;
  //     return true;
  //   },
  //   jwt({ token, trigger, session, user }) {
  //     if (user) {
  //       token = { ...token, access_token: (user as any).access_token };
  //     }

  //     if (trigger === "update") token.name = session.user.name;
  //     return token;
  //   },
  //   async signIn(params) {
  //     const { account, user } = params;
  //     if (account?.provider === "github") {
  //       try {
  //         const res = await axios.post(
  //           `${process.env.NEXT_PUBLIC_BACKEND_URL}/api/validate-token`,
  //           { token: account.access_token },
  //           {
  //             headers: {
  //               "Content-Type": "application/json",
  //             },
  //           }
  //         );

  //         (user as any).access_token = res.data.token;
  //       } catch (error: any) {
  //         console.log(error.message, process.env.NEXT_PUBLIC_BACKEND_URL);
  //         return false;
  //       }

  //       return true;
  //     }

  //     return false;
  //   },
  //   async session({ session, token }) {
  //     session.access_token = token.access_token;

  //     return session;
  //   },
  // },
  experimental: {
    enableWebAuthn: true,
  },
  debug: process.env.NODE_ENV !== "production" ? true : false,
} satisfies NextAuthConfig;

export const { handlers, auth, signIn, signOut } = NextAuth(config);

declare module "next-auth" {
  interface Session {
    access_token?: string;
  }
}

declare module "next-auth/jwt" {
  interface JWT {
    access_token?: string;
  }
}
