"use client";

import * as React from "react";
import * as DropdownMenuPrimitive from "@radix-ui/react-dropdown-menu";
import { Check, ChevronRight, Circle } from "lucide-react";

import { cn } from "@/lib/utils";

const {
  Root: DropdownMenu,
  Trigger: DropdownMenuTrigger,
  Group: DropdownMenuGroup,
  Portal: DropdownMenuPortal,
  Sub: DropdownMenuSub,
  RadioGroup: DropdownMenuRadioGroup,
  SubTrigger: PrimitiveSubTrigger,
  SubContent: PrimitiveSubContent,
  Content: PrimitiveContent,
  Item: PrimitiveItem,
  CheckboxItem: PrimitiveCheckboxItem,
  RadioItem: PrimitiveRadioItem,
  Label: PrimitiveLabel,
  Separator: PrimitiveSeparator,
  ItemIndicator,
} = DropdownMenuPrimitive;

const utilityClasses = {
  item: "relative flex cursor-default select-none items-center rounded-sm text-sm outline-none transition-colors",
  common: "data-[disabled]:pointer-events-none data-[disabled]:opacity-50",
  subContent: "z-50 min-w-[8rem] overflow-hidden rounded-md border bg-popover p-1 text-popover-foreground shadow-lg",
  content: "z-50 min-w-[8rem] overflow-hidden rounded-md border bg-popover p-1 text-popover-foreground shadow-md",
  indicator: "absolute left-2 flex h-3.5 w-3.5 items-center justify-center",
};

const DropdownMenuSubTrigger = React.forwardRef<
  React.ElementRef<typeof PrimitiveSubTrigger>,
  React.ComponentPropsWithoutRef<typeof PrimitiveSubTrigger> & { inset?: boolean }
>(({ className, inset, children, ...restProps }, ref) => (
  <PrimitiveSubTrigger
    ref={ref}
    className={cn(
      "flex cursor-default select-none items-center rounded-sm px-2 py-1.5 text-sm outline-none focus:bg-accent data-[state=open]:bg-accent",
      inset && "pl-8",
      className
    )}
    {...restProps}
  >
    {children}
    <ChevronRight className="ml-auto h-4 w-4" />
  </PrimitiveSubTrigger>
));

const DropdownMenuSubContent = React.forwardRef<
  React.ElementRef<typeof PrimitiveSubContent>,
  React.ComponentPropsWithoutRef<typeof PrimitiveSubContent>
>(({ className, ...restProps }, ref) => (
  <PrimitiveSubContent
    ref={ref}
    className={cn(
      utilityClasses.subContent,
      "data-[state=open]:animate-in data-[state=closed]:animate-out",
      className
    )}
    {...restProps}
  />
));

const DropdownMenuContent = React.forwardRef<
  React.ElementRef<typeof PrimitiveContent>,
  React.ComponentPropsWithoutRef<typeof PrimitiveContent>
>(({ className, sideOffset = 4, ...restProps }, ref) => (
  <DropdownMenuPortal>
    <PrimitiveContent
      ref={ref}
      sideOffset={sideOffset}
      className={cn(
        utilityClasses.content,
        "data-[state=open]:animate-in data-[state=closed]:animate-out",
        className
      )}
      {...restProps}
    />
  </DropdownMenuPortal>
));

const DropdownMenuItem = React.forwardRef<
  React.ElementRef<typeof PrimitiveItem>,
  React.ComponentPropsWithoutRef<typeof PrimitiveItem> & { inset?: boolean }
>(({ className, inset, ...restProps }, ref) => (
  <PrimitiveItem
    ref={ref}
    className={cn(
      utilityClasses.item,
      "px-2 py-1.5 focus:bg-accent focus:text-accent-foreground",
      inset && "pl-8",
      className
    )}
    {...restProps}
  />
));

const DropdownMenuCheckboxItem = React.forwardRef<
  React.ElementRef<typeof PrimitiveCheckboxItem>,
  React.ComponentPropsWithoutRef<typeof PrimitiveCheckboxItem>
>(({ className, children, checked, ...restProps }, ref) => (
  <PrimitiveCheckboxItem
    ref={ref}
    className={cn(
      utilityClasses.item,
      "py-1.5 pl-8 pr-2 focus:bg-accent focus:text-accent-foreground",
      className
    )}
    checked={checked}
    {...restProps}
  >
    <span className={utilityClasses.indicator}>
      <ItemIndicator>
        <Check className="h-4 w-4" />
      </ItemIndicator>
    </span>
    {children}
  </PrimitiveCheckboxItem>
));

const DropdownMenuRadioItem = React.forwardRef<
  React.ElementRef<typeof PrimitiveRadioItem>,
  React.ComponentPropsWithoutRef<typeof PrimitiveRadioItem>
>(({ className, children, ...restProps }, ref) => (
  <PrimitiveRadioItem
    ref={ref}
    className={cn(
      utilityClasses.item,
      "py-1.5 pl-8 pr-2 focus:bg-accent focus:text-accent-foreground",
      className
    )}
    {...restProps}
  >
    <span className={utilityClasses.indicator}>
      <ItemIndicator>
        <Circle className="h-2 w-2 fill-current" />
      </ItemIndicator>
    </span>
    {children}
  </PrimitiveRadioItem>
));

const DropdownMenuLabel = React.forwardRef<
  React.ElementRef<typeof PrimitiveLabel>,
  React.ComponentPropsWithoutRef<typeof PrimitiveLabel> & { inset?: boolean }
>(({ className, inset, ...restProps }, ref) => (
  <PrimitiveLabel
    ref={ref}
    className={cn("px-2 py-1.5 text-sm font-semibold", inset && "pl-8", className)}
    {...restProps}
  />
));

const DropdownMenuSeparator = React.forwardRef<
  React.ElementRef<typeof PrimitiveSeparator>,
  React.ComponentPropsWithoutRef<typeof PrimitiveSeparator>
>(({ className, ...restProps }, ref) => (
  <PrimitiveSeparator
    ref={ref}
    className={cn("-mx-1 my-1 h-px bg-muted", className)}
    {...restProps}
  />
));

const DropdownMenuShortcut: React.FC<React.HTMLAttributes<HTMLSpanElement>> = ({ className, ...props }) => (
  <span className={cn("ml-auto text-xs tracking-widest opacity-60", className)} {...props} />
);

export {
  DropdownMenu,
  DropdownMenuTrigger,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuCheckboxItem,
  DropdownMenuRadioItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuGroup,
  DropdownMenuPortal,
  DropdownMenuSub,
  DropdownMenuSubContent,
  DropdownMenuSubTrigger,
  DropdownMenuRadioGroup,
};
