import React from "react";

const UserNode = ({ node }: { node: any }) => {
  const hasChildren = node.children && node.children.length > 0;
  return (
    <div className={"flex flex-col border border-gray-500 m-2 rounded-sm"}>
      <button className="m-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
        <a href={`/assign-user/${node.id}`}>{node.username}</a>
      </button>
      {hasChildren && (
        <div className="ml-4">
          {node.children.map((child: any) => (
            <UserNode key={child.id} node={child} />
          ))}
        </div>
      )}
    </div>
  );
};

const UserHierarchy = ({ node }: { node: any }) => {
  return (
    <div className="flex flex-col items-center">
      <UserNode node={node} />
    </div>
  );
};

export default UserHierarchy;
