'use client';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { UserForm } from './user-form';

export default function AssignUser({ user }: { user: string }) {
  const { data: session, status } = useSession();
  const [apiResponse, setApiResponse] = useState([]);
  const [stateCurrentUser, setStateCurrentUser] = useState(null as any);
  const router = useRouter();

  useEffect(() => {
    makeRequestWithToken();
    makeUserRequestWithToken();
  }, []);

  const makeRequestWithToken = async () => {
    try {
      const response = await fetch('/api/proxy', {
        method: 'POST',
        body: JSON.stringify({
          method: 'GET',
          authorization: `Bearer ${session?.access_token}`,
          url: '/api/users',
        }),
      });
      const data = await response.json();
      setApiResponse(data);
    } catch (error) {
      setApiResponse([]);
    }
  };
  const makeUserRequestWithToken = async () => {
    try {
      const response = await fetch('/api/proxy', {
        method: 'POST',
        body: JSON.stringify({
          method: 'GET',
          body: { id: user },
          authorization: `Bearer ${session?.access_token}`,
          url: '/api/user',
        }),
      });
      const data = await response.json();
      if (!data) router.push('/users');

      setStateCurrentUser(data);
    } catch (error) {
      setStateCurrentUser(null);
    }
  };
  const makeAssignRequestWithToken = async (parentId: string | null) => {
    try {
      const response = await fetch('/api/proxy', {
        method: 'POST',
        body: JSON.stringify({
          method: 'PATCH',
          body: { parentId },
          authorization: `Bearer ${session?.access_token}`,
          url: `/api/user/${user}/parent`,
        }),
      });
      if (response.status !== 200) {
        toast.error('Failed to modify the boss', { position: 'top-center' });
      } else {
        toast.success('Boss assignment updated', { position: 'top-center' });
      }
      makeUserRequestWithToken();
    } catch (error) {}
  };
  const handleDelete = async () => {
    try {
      const response = await fetch('/api/proxy', {
        method: 'POST',
        body: JSON.stringify({
          method: 'DELETE',
          authorization: `Bearer ${session?.access_token}`,
          url: `/api/user/${user}`,
        }),
      });
      if (response.status !== 200) {
        toast.error('Failed to delete the user', { position: 'top-center' });
        makeUserRequestWithToken();
      } else {
        toast.success('User deleted', { position: 'top-center' });
        router.push('/users');
      }
    } catch (error) {}
  };

  const handleClick = (userId: string | null) => {
    makeAssignRequestWithToken(userId);
  };

  return (
    <div className="flex flex-col gap-4">
      {stateCurrentUser !== null && (
        <>
          <h1 className="text-3xl font-bold">
            User {stateCurrentUser?.username ?? user}
          </h1>
          {/* <div className="flex flex-col gap-4 p-4 bg-gray-100 rounded-md"> */}
          <UserForm data={stateCurrentUser} />
          {/* </div> */}
        </>
      )}
      <h1 className="text-3xl font-bold">Delete the user</h1>
      <p>
        The user will be wiped out of the database. Operation cannot be
        reverted.
      </p>
      <button
        className="border border-pink-300 rounded-md px-4 py-2 text-gray-700 bg-pink-200 hover:border-gray-300 hover:bg-gray-300 focus:outline-none focus:ring focus:ring-gray-300"
        onClick={() => handleDelete()}
      >
        Delete user
      </button>
      <h1 className="text-3xl font-bold">Unlink the boss</h1>
      <button
        className="border border-red-300 rounded-md px-4 py-2 text-gray-700 bg-red-200 hover:border-gray-300 hover:bg-gray-300 focus:outline-none focus:ring focus:ring-gray-300"
        onClick={() => handleClick(null)}
      >
        Unlink current boss
      </button>
      <h1 className="text-3xl font-bold">Choose new boss</h1>
      <div className="flex flex-col gap-4 p-4 bg-gray-100 rounded-md">
        <div className="flex flex-wrap gap-2">
          {apiResponse.map((user: any, index) => (
            <button
              key={`bosses-${index + 1}`}
              className="border border-gray-300 rounded-md px-4 py-2 text-gray-700 hover:bg-gray-300 focus:outline-none focus:ring focus:ring-gray-300"
              onClick={() => handleClick(user.id)}
            >
              {user.username}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
}
