'use client';

import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import UserHierarchy from './hierarchy';
import { convertFlatArrayToTree } from '@/lib/tree-convert';

export default function Users() {
  const { data: session } = useSession();
  const [apiResponse, setApiResponse] = useState([]);

  useEffect(() => {
    makeRequestWithToken();
  }, []);

  const makeRequestWithToken = async () => {
    try {
      const response = await fetch('/api/proxy', {
        method: 'POST',
        body: JSON.stringify({
          method: 'GET',
          authorization: `Bearer ${session?.access_token}`,
          url: '/api/users',
        }),
      });
      const data = await response.json();
      setApiResponse(data);
    } catch (error) {
      setApiResponse([]);
    }
  };
  const topNodes = convertFlatArrayToTree(apiResponse);
  return (
    <div className="flex flex-col gap-4">
      <h1 className="text-3xl font-bold">All users</h1>
      <p>Boss - subordinate hierarchy</p>
      <div className="flex flex-col gap-4 p-4 bg-gray-100 rounded-md">
        <div className="flex flex-wrap gap-2">
          {topNodes.map((node, key) => (
            <UserHierarchy key={key} node={node} />
          ))}
        </div>
      </div>
    </div>
  );
}
