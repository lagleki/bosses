import React from 'react';

export const UserForm = ({ data }: { data: any }) => {
  return (
    <form className="p-6 bg-white shadow-md rounded-md">
      <h2 className="text-xl font-semibold mb-4">User Information</h2>
      <div className="mb-4">
        <label className="block text-gray-700">ID:</label>
        <input
          type="text"
          readOnly
          name="id"
          value={data.id}
          className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
        />
      </div>
      <div className="mb-4">
        <label className="block text-gray-700">Username:</label>
        <input
          type="text"
          readOnly
          name="username"
          value={data.username}
          className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
        />
      </div>
      <div className="mb-4">
        <label className="block text-gray-700">Provider:</label>
        <input
          type="text"
          readOnly
          name="provider"
          value={data.provider}
          className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
        />
      </div>
      <div className="mb-4">
        <label className="block text-gray-700">Email:</label>
        <input
          type="text"
          readOnly
          name="email"
          value={data.email}
          className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
        />
      </div>
      <div className="mb-4">
        <label className="block text-gray-700">Token:</label>
        <input
          type="text"
          readOnly
          name="token"
          value={data.token || ''}
          className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
        />
      </div>
      {data.parent && (
        <>
          <h2 className="text-xl font-semibold mt-6 mb-4">
            Parent Information
          </h2>
          <div className="mb-4">
            <label className="block text-gray-700">Parent ID:</label>
            <input
              type="text"
              readOnly
              name="id"
              value={data.parent.id}
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Parent Username:</label>
            <input
              type="text"
              readOnly
              name="username"
              value={data.parent.username}
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Parent Provider:</label>
            <input
              type="text"
              readOnly
              name="provider"
              value={data.parent.provider || ''}
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Parent Email:</label>
            <input
              type="text"
              readOnly
              name="email"
              value={data.parent.email}
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Parent Token:</label>
            <input
              type="text"
              readOnly
              name="token"
              value={data.parent.token || ''}
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500"
            />
          </div>
        </>
      )}
    </form>
  );
};
