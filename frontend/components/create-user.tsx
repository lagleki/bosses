'use client';

import { useSession } from 'next-auth/react';
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { toast } from 'react-toastify';

export default function CreateUser() {
  const { data: session } = useSession();

  // State variables for username and email
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const router = useRouter();

  // Handle form submission
  const handleSubmit = (event: { preventDefault: () => void }) => {
    event.preventDefault();
    makeRequestWithToken();
  };

  const makeRequestWithToken = async () => {
    try {
      const response = await fetch('/api/proxy', {
        method: 'POST',
        body: JSON.stringify({
          method: 'POST',
          body: { username, email, password },
          authorization: `Bearer ${session?.access_token}`,
          url: '/api/user',
        }),
      });
      if (response.status !== 200) {
        toast.error('Failed to create a user', { position: 'top-center' });
      } else {
        toast.success('User created', { position: 'top-center' });
      }
    } catch (error) {}
    router.push('/users');
  };

  return (
    <div className="flex flex-col gap-4">
      <h1 className="text-3xl font-bold">
        Create user via "dashboard admin" provider
      </h1>
      <div className="flex flex-col gap-4 p-4 bg-gray-100 rounded-md">
        <pre>
          <form onSubmit={handleSubmit} className="max-w-md mx-auto">
            <div className="mb-4">
              <label
                htmlFor="username"
                className="block text-gray-700 font-bold mb-2"
              >
                Username
              </label>
              <input
                type="text"
                id="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                placeholder="Enter username"
                required
              />
            </div>
            {/* <div className="mb-6">
              <label
                htmlFor="email"
                className="block text-gray-700 font-bold mb-2"
              >
                Email
              </label>
              <input
                type="email"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                placeholder="Enter email"
                required
              />
            </div> */}
            <div className="mb-6">
              <label
                htmlFor="password"
                className="block text-gray-700 font-bold mb-2"
              >
                Password
              </label>
              <input
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                placeholder="Enter password"
                required
              />
            </div>
            <div className="text-center">
              <button
                type="submit"
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              >
                Create User
              </button>
            </div>
          </form>
        </pre>
      </div>
    </div>
  );
}
