interface TreeNode {
  id: number;
  username: string;
  parentId?: number; // Optional because some nodes might not have a parent
  children?: TreeNode[];
}

// Recursive helper function to build the tree
function buildTree(
  nodes: TreeNode[],
  parentId: number | null,
  parent?: TreeNode
): TreeNode[] {
  const children: TreeNode[] = [];

  for (let node of nodes) {
    if (node.parentId === parentId) {
      children.push(node);

      // Recursively build the subtree for this node
      const subtree = buildTree(nodes, node.id);
      if (subtree.length > 0) {
        node.children = subtree; // Add children to the current node
      }
    }
  }

  if (parent) {
    parent.children = children;
    return [parent];
  }
  return children;
}

// Main function to convert the flattened array to a tree
export function convertFlatArrayToTree(flattenedTree: TreeNode[]): TreeNode[] {
  try {
    const rootNodes: TreeNode[] = [];
    const nodesMap: { [key: number]: TreeNode } = {};

    // Populate the map with all nodes
    for (let node of flattenedTree ?? []) {
      nodesMap[node.id] = node;
    }

    // Find the root nodes (those without a parentId)
    for (const id in nodesMap) {
      if (!nodesMap[id].parentId) {
        rootNodes.push(nodesMap[id]);
      }
    }

    // Build the tree starting from the root nodes
    let tree: TreeNode[] = [];
    for (let rootNode of rootNodes) {
      tree = [...tree, ...buildTree(flattenedTree, rootNode.id, rootNode)];
    }

    return tree;
  } catch (error) {
    return [];
  }
}
