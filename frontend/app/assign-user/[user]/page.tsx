import { auth } from "auth";
import Users from "@/components/users";
import { SessionProvider } from "next-auth/react";
import AssignUser from "@/components/assign-user";
import { useRouter } from "next/router";
import { redirect } from "next/navigation";

export default async function ClientPage({
  params,
}: {
  params: Record<string, string>;
}) {
  const session = await auth();
  if (session?.user) {
    // TODO: Look into https://react.dev/reference/react/experimental_taintObjectReference
    // filter out sensitive data before passing to client.
    session.user = {
      name: session.user.name,
      email: session.user.email,
      image: session.user.image,
    };
  } else {
    redirect("/");
  }

  return (
    <SessionProvider basePath={"/auth"} session={session}>
      <AssignUser user={params.user} />
    </SessionProvider>
  );
}
