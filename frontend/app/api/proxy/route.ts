import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";
import { NextResponse } from "next/server";

export async function POST(req: any, res: NextApiResponse) {
  try {
    const body = await req.json();

    // Make the request to the external API
    console.log(`${process.env.NEXT_PUBLIC_BACKEND_URL}${body.url as string}`, {
      method: body.method,
      data: body.body,
      params: body.params,
      headers: {
        Authorization: body.authorization,
        "Content-Type": "application/json",
      },
    });
    const response = await axios(
      `${process.env.NEXT_PUBLIC_BACKEND_URL}${body.url as string}`,
      {
        method: body.method,
        data: body.body,
        params: body.params,
        headers: {
          Authorization: body.authorization,
          "Content-Type": "application/json",
        },
      }
    );
    return new NextResponse(JSON.stringify(response.data), {
      status: 200,
      headers: { "content-type": "application/json" },
    });
  } catch (error) {
    console.log((error as any).message);
    return new NextResponse(
      JSON.stringify({
        success: false,
        message: "server behind proxy errored out",
      }),
      { status: 503, headers: { "content-type": "application/json" } }
    );
  }
}
