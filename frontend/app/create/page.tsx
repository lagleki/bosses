import { auth } from "auth";
import CreateUser from "@/components/create-user";
import { SessionProvider } from "next-auth/react";
import { redirect } from "next/navigation";

export default async function ClientPage() {
  const session = await auth();
  if (session?.user) {
    // TODO: Look into https://react.dev/reference/react/experimental_taintObjectReference
    // filter out sensitive data before passing to client.
    session.user = {
      name: session.user.name,
      email: session.user.email,
      image: session.user.image,
    };
  }
  // else{
  //   redirect("/");

  // }

  return (
    <SessionProvider basePath={"/auth"} session={session}>
      <CreateUser />
    </SessionProvider>
  );
}
