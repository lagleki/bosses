import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import axios from 'axios';
import { AuthRepository } from '../db/repositories';
import { AuthEntity } from '../schemas';

@Injectable()
export class AppService {
  constructor(private readonly authRepository: AuthRepository) {}

  async getUserData(token: string): Promise<any> {
    const response = await axios.get('https://api.github.com/user', {
      headers: {
        Authorization: `token ${token}`,
      },
    });
    return response.data;
  }

  async validateToken(token: string): Promise<AuthEntity> {
    if (!token)
      throw new HttpException(
        'token failed validation',
        HttpStatus.UNAUTHORIZED
      );

    try {
      const user = await this.authRepository.findUserByToken(token);
      if (user) return user;
      const data = await this.getUserData(token);
      return await this.authRepository.upsertUser(data.login, {
        token,
        email: data.email,
        provider: 'github'
      });
    } catch (error) {
      throw new HttpException(
        'token failed validation',
        HttpStatus.UNAUTHORIZED
      );
    }
  }
}
