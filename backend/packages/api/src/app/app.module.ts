import { Module } from '@nestjs/common';
import { CacheModule } from '@nestjs/cache-manager';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DbModule } from '../db/db.module';
import { AuthRepository } from '../db/repositories';
import { AuthGuard } from './app.guard';

@Module({
  imports: [
    CacheModule.register({
      ttl: 300 * 1000, // milliseconds, 5 minutes
      max: 100, // maximum number of items in cache
    }),
    DbModule,
  ],
  controllers: [AppController],
  providers: [AppService, AuthRepository, AuthGuard],
  exports: [AppService],
})
export class AppModule {}
