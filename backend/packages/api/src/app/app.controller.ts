import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseFilters,
  Delete,
  UseGuards,
} from '@nestjs/common';

import { AppService } from './app.service';
import { HttpExceptionsFilter } from './exception.filter';
import { AuthRepository } from '../db/repositories';
import { AuthEntity } from '../schemas';
import { AuthGuard } from './app.guard';
import {
  AuthLookupDto,
  AuthValidateLoginCredsDto,
  CreateAuthDto,
} from '../dtos';

@UseFilters(HttpExceptionsFilter)
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthRepository
  ) {}

  @Get('health')
  async healthCheck() {
    return 'ok';
  }
  // @UseGuards(AuthGuard)
  @Get('users')
  async listAllUsers(): Promise<AuthEntity[]> {
    return await this.authService.findAll();
  }

  // @UseGuards(AuthGuard)
  @Get('user')
  async getUser(@Body() body: AuthLookupDto): Promise<AuthEntity> {
    return this.authService.findUser(body);
  }

  @Post('validate-user-creds')
  async getUserByCreds(
    @Body() body: AuthValidateLoginCredsDto
  ): Promise<{ status: 'ok'; user: AuthEntity }> {
    return { status: 'ok', user: await this.authService.findUser(body) };
  }

  // @UseGuards(AuthGuard)
  @Post('user')
  async createUserViaAdmin(
    @Body() createUserDto: CreateAuthDto
  ): Promise<AuthEntity> {
    const { username, email, password } = createUserDto;
    return this.authService.createAuthViaAdmin({ username, email, password });
  }

  @Post('validate-token')
  async validateUserToken(@Body('token') token: string): Promise<AuthEntity> {
    return this.appService.validateToken(token);
  }

  // @UseGuards(AuthGuard)
  @Patch('user/:id/parent')
  async assignParent(
    @Param('id') id: string,
    @Body('parentId') parentId: string | null
  ): Promise<AuthEntity> {
    return this.authService.assignParent(id, parentId);
  }

  // @UseGuards(AuthGuard)
  @Delete('user/:id')
  async deleteUser(@Param('id') id: string): Promise<void> {
    return this.authService.deleteUser(id);
  }
}
