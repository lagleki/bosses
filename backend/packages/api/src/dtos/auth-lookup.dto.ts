import { IsOptional } from 'class-validator';

export class AuthLookupDto {
  @IsOptional()
  id?: string;

  @IsOptional()
  username?: string;

  @IsOptional()
  email?: string;

  @IsOptional()
  token?: string;
}
