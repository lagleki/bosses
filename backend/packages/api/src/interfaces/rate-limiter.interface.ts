export interface RateLimitOptions {
  checkForAuthOtpVerification?: boolean;
  checkOtpRequest?: boolean;
}

export interface LimitResponse {
  status: boolean;
  reason?: string;
  nextAttemptIn?: number;
}
