export interface AuthLookup {
  id?: string;
  username?: string;
  password?: string;
  email?: string;
  token?: string;
}

export interface AuthTokenResponse {
  accessToken?: string;
  expiresIn?: number;
  refreshToken?: string;
  scope?: string;
  tokenType?: string;
}
