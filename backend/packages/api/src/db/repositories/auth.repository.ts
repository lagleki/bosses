import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateAuthDto } from '../../dtos';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthEntity } from '../../schemas/auth.schema';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional';
import { AuthLookup } from '../../interfaces/auth.interface';

@Injectable()
export class AuthRepository {
  constructor(
    @InjectRepository(AuthEntity)
    private readonly authRepository: Repository<AuthEntity>
  ) {}

  async findAll(): Promise<AuthEntity[]> {
    const query = `
        WITH RECURSIVE user_hierarchy AS (
          SELECT
            id,
            username,
            "parentId"
          FROM
            auth
          WHERE
            "parentId" IS NULL
  
          UNION ALL
  
          SELECT
            a.id,
            a.username,
            a."parentId"
          FROM
            auth a
          INNER JOIN
            user_hierarchy uh ON a."parentId" = uh.id
        )
        SELECT
          id,
          username,
          "parentId"
        FROM
          user_hierarchy;
      `;

    return await this.authRepository.query(query);
  }

  @Transactional()
  async createAuthViaAdmin(input: CreateAuthDto): Promise<AuthEntity> {
    try {
      const auth = new AuthEntity();
      auth.username = input.username;
      auth.email = input.email;
      auth.password = input.password;
      auth.token = input.token;
      auth.provider = 'admin';

      if (input.parentId) {
        const parent = await this.authRepository.findOne({
          where: { id: input.parentId },
        });
        if (parent) auth.parent = parent;
      }

      return await this.authRepository.save(auth);
    } catch (error) {
      throw new HttpException(
        'failed to create auth account',
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  @Transactional()
  async upsertUser(
    username: string,
    data: { token?: string; email?: string; provider: string }
  ): Promise<AuthEntity> {
    try {
      const user: Partial<AuthEntity> = { username, provider: data.provider };
      if (data.token !== undefined) user.token = data.token;
      if (data.email !== undefined) user.email = data.email;
      const newUser = this.authRepository.create(user);
      const existingUser = await this.authRepository.findOne({
        where: { username, provider: data.provider },
      });

      if (existingUser) {
        await this.authRepository.update({ username }, newUser);
        return await this.authRepository.findOne({
          where: { username, provider: data.provider },
        });
      }
      return await this.authRepository.save(newUser);
    } catch (error: any) {
      console.log(error.message);
      throw new HttpException(
        'failed to save auth account',
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  @Transactional()
  async findUser(input: AuthLookup): Promise<AuthEntity> {
    try {
      const query = this.authRepository.createQueryBuilder('auth');

      if (input.username) {
        query.andWhere('auth.username = :username', {
          username: input.username,
        });
      }
      if (input.password) {
        //TODO: add salt handling
        query.andWhere('auth.password = :password', {
          password: input.password,
        });
      }
      if (input.id) {
        query.andWhere('auth.id = :id', {
          id: input.id,
        });
      }
      if (input.email) {
        query.andWhere('auth.email = :email', { email: input.email });
      }

      if (input.token) {
        query.andWhere('auth.token = :token', { token: input.token });
      }

      return await query.leftJoinAndSelect('auth.parent', 'parent').getOne();
    } catch (error) {
      throw new HttpException(
        'failed to find auth account',
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  async findUserByToken(token: string): Promise<AuthEntity> {
    try {
      return await this.authRepository.findOne({ where: { token } });
    } catch (error) {
      throw new HttpException(
        'failed to findById auth account',
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  async assignParent(id: string, parentId: string): Promise<AuthEntity> {
    if (id === parentId)
      throw new HttpException(
        'assigning itself as a boss forbidden',
        HttpStatus.FORBIDDEN
      );
    const user = await this.authRepository.findOne({
      where: { id },
      relations: ['parent'],
    });
    if (!user) throw new NotFoundException('User not found');
    if (parentId === null) {
      user.parent = null;
    } else {
      const parent = await this.authRepository.findOne({
        where: { id: parentId },
      });
      if (!parent) throw new NotFoundException('Parent not found');

      user.parent = parent;
    }
    return this.authRepository.save(user);
  }

  async deleteUser(id: string): Promise<void> {
    const result = await this.authRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException('User not found');
    }
  }
}
