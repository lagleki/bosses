import { Entity, PrimaryGeneratedColumn, Column, Index, ManyToOne, OneToMany } from 'typeorm';

@Entity('auth')
export class AuthEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  @Index()
  username: string;
  
  @Column({ nullable: true })
  password?: string;

  @Index()
  @Column({ nullable: true })
  provider?: string;

  @Index()
  @Column({ nullable: true })
  email?: string;

  @Index()
  @Column({ nullable: true })
  token?: string;

  @ManyToOne(() => AuthEntity, auth => auth.children, { nullable: true })
  parent?: AuthEntity;

  @OneToMany(() => AuthEntity, auth => auth.parent)
  children: AuthEntity[];
}
