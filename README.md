# How to install

For now OAuth is disabled. Vulnerabilities present:
* no salt for passwords
* no ddos protection and rate limiting
* signup/login is done via creds (username+password)
    * OAuth providers are to be added on demand.

# Some questions
* better use OAuth and validate tokens from the backend side
* tokens received from OAuth are stored in the backend db and optionally additionally in in-memory cache (Redis would be better)

# To develop locally

* install Node version manager (nvm)
* `nvm use 21`
* make sure pnpm and npx are installed
* create .env file from .env.example
* `make local-up` to boot up posgresql locally
* `cd backend && npm i` to install dependencies for the backend
* from `backend` directory `npx nx serve api` to run `api` microservice
* `cd frontend && pnpm i`  to install dependencies for the frontend
* from `frontend` directory `npm run dev` to run the frontend in dev mode
* open http://localhost:3000 and create a new user. remember your password to login later

# ToDo:

* deploy via terraform to save time
* ci/cd
* AWS adaptation
* better readme